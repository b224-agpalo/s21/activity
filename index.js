
/*
    1. Create a function which is able to receive a single argument and add the input at the end of the users array.
        -function should be able to receive a single argument.
        -add the input data at the end of the array.
        -The function should not be able to return data.
        -invoke and add an argument to be passed in the function.
        -log the users array in the console.

*/
let musicalInstruments = ["Guitar", "Piano", "Drums"];

function musicFunction(instrumentName) {
    musicalInstruments[musicalInstruments.length] = instrumentName;
    console.log(musicalInstruments);
}

musicFunction("Cajon");
console.log(musicalInstruments);
musicFunction("Microphone");
console.log(musicalInstruments);


/*
    2. Create function which is able to receive an index number as a single argument return the item accessed by its index.
        -function should be able to receive a single argument.
        -return the item accessed by the index.
        -Create a global variable called outside of the function called itemFound and store the value returned by the function in it.
        -log the itemFound variable in the console.

*/

function getMusic(index) {
    return musicalInstruments[index];
};

let itemOut = getMusic(1);
console.log(itemOut);

/*
    3. Create function which is able to delete the last item in the array and return the deleted item.
        -Create a function scoped variable to store the last item in the users array.
        -Shorten the length of the array by at least 1 to delete the last item.
        -return the last item in the array which was stored in the variable.

*/
function deleteLastItem(deletedItem) {
    console.log(deletedItem);
    musicalInstruments.length--;
    return deletedItem;
};

let newItemList = deleteLastItem(musicalInstruments[musicalInstruments.length - 1]);
console.log(newItemList);

/*
    4. Create function which is able to update a specific item in the array by its index.
        -Function should be able to receive 2 arguments, the update and the index number.
        -First, access and locate the item by its index then re-assign the item with the update.
        -This function should not have a return.
        -Invoke the function and add the update and index number as arguments.
        -log the users array in the console.

*/
function updateFunction(instrumentName, musicIndex) {
    musicalInstruments[musicIndex] = instrumentName;
    console.log(musicalInstruments);
}

updateFunction("Flute", 1);
console.log(musicalInstruments);
/*
    5. Create function which is able to delete all items in the array.
        -You can modify/set the length of the array.
        -The function should not return anything.

*/

function deleteAlll(deletedItem) {
    musicalInstruments.length = 0;

};

deleteAlll();
console.log(musicalInstruments.length);

/*
    6. Create a function which is able to check if the array is empty.
        -Add an if statement to check if the length of the users array is greater than 0.
            -If it is, return false.
        -Else, return true.
        -Create a global variable called outside of the function  called isUsersEmpty and store the returned value from the function.
        -log the isUsersEmpty variable in the console.

*/

function emptyCheck(isItEmpty) {
    if (isItEmpty == false) {
        console.log("The array is not empty");
    }
    else { console.log("It is empty!"); }
    console.log(musicalInstruments);

};

let isUsersEmpty = emptyCheck(musicalInstruments.length == 0);


